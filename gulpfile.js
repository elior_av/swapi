var gulp = require('gulp');
var browserSync = require('browser-sync');
var $ = require('gulp-load-plugins')();
var config = require('./gulp.config')();
var del = require('del');


gulp.task('browser-sync', function () {
    startBrowserSync(true);
});

gulp.task('css', function () {
    return gulp.src('app/css/*.css')
        .pipe(browserSync.stream());
});

gulp.task('wiredep', function () {
    var options = config.getWiredepDefaultOptions();
    var wiredep = require('wiredep').stream;
    return gulp
        .src(config.index)
        .pipe(wiredep(options))
        .pipe($.inject(gulp.src(config.js)))
        .pipe($.inject(gulp.src(config.css)))
        .pipe(gulp.dest(config.root));
});

gulp.task('templatecache',['clean-temp'], function() {
    log('Creating an AngularJS $templateCache');

    return gulp
        .src(config.htmltemplates)
        .pipe($.minifyHtml({empty: true}))
        .pipe($.angularTemplatecache(
            config.templateCache.file,
            config.templateCache.options
        ))
        .pipe(gulp.dest(config.temp + 'js/'));
});

gulp.task('serve-dev',['wiredep'], function () {
    startBrowserSync(true);
});

gulp.task('serve-build',['build'], function () {
    startBrowserSync(false);
});

gulp.task('build', ['optimize'], function() {
    del(config.temp);
});

gulp.task('fonts', ['clean-fonts'], function() {
    log('Copying fonts');

    return gulp
        .src(config.fonts)
        .pipe($.flatten())
        .pipe(gulp.dest(config.build + 'fonts'));
});

gulp.task('optimize', ['clean','templatecache','fonts'], function() {
    log('Optimizing the js, css, and html');

    // Filters are named for the gulp-useref path
    var cssFilter = $.filter(['**/*.css'], {restore: true});
    var jsFilter = $.filter(['**/*.js'], {restore: true});
    var indexHtmlFilter = $.filter(['**/*', '!**/index.html'], { restore: true });
    var templateCache = config.temp + 'js/' + config.templateCache.file;

    return gulp
        .src(config.index)
        .pipe($.plumber())
        .pipe($.inject(gulp.src(templateCache, {read: false}), {starttag: '<!-- inject:templates:js -->'}))
        // Apply the concat and file replacement with useref
        .pipe($.useref({ searchPath: config.client }))
        // Get the css
        .pipe(cssFilter)
        .pipe($.csso())
        .pipe(cssFilter.restore)
        // Get the custom javascript
        .pipe(jsFilter)
        .pipe($.ngAnnotate({add: true}))
        .pipe($.uglify())
        //strip comments
        .pipe($.stripComments())
        .pipe(jsFilter.restore)
        // Take inventory of the file names for future rev numbers
        .pipe(indexHtmlFilter)
        .pipe($.rev())
        // // Replace the file names in the html with rev numbers
        .pipe(indexHtmlFilter.restore)
        .pipe($.revReplace({replaceInExtensions: ['.html']}))
        
        .pipe(gulp.dest(config.build));
});

gulp.task('clean', function() {
    log('Cleaning: ' + $.util.colors.blue(config.build));
    return del(config.build);
});
gulp.task('clean-temp', function() {
    log('Cleaning: ' + $.util.colors.blue(config.temp));
    return del(config.temp);
});
gulp.task('clean-fonts', function() {
    return clean(config.build + 'fonts/**/*.*');
});

gulp.task('browserSyncReload', ['optimize'], browserSync.reload);

function startBrowserSync(isDev) {
    if (isDev) {
        gulp.watch(config.css, ['css']);
        gulp.watch([config.js, config.html]).on('change', browserSync.reload);
    }
    else {
        gulp.watch([config.js, config.html], ['browserSyncReload'])
            .on('change', changeEvent);
    }
    var options = {
        server: {
            baseDir: isDev ? config.root : config.build,
            index: "index.html"
        },
        port: 3000,
        ghostMode: { // these are the defaults t,f,t,t
            clicks: true,
            location: false,
            forms: true,
            scroll: true
        },
        injectChanges: true,
        logFileChanges: true,
        logLevel: 'debug',
        logPrefix: 'gulp-patterns',
        notify: true,
        reloadDelay: 1000
    };
    browserSync(options);
}

function log(msg) {
    if (typeof(msg) === 'object') {
        for (var item in msg) {
            if (msg.hasOwnProperty(item)) {
                $.util.log($.util.colors.blue(msg[item]));
            }
        }
    } else {
        $.util.log($.util.colors.blue(msg));
    }
}

function clean(path) {
    log('Cleaning: ' + $.util.colors.blue(path));
    return del(path);
}

function changeEvent(event) {
    var srcPattern = new RegExp('/.*(?=/' + config.source + ')/');
    log('File ' + event.path.replace(srcPattern, '') + ' ' + event.type);
}