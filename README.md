# Installation:
First, you need to clone this repository
```
git clone https://elior_av@bitbucket.org/elior_av/swapi.git

```
then, you need to install npm and bower dependencies
```
npm install

bower insatll

```
finally, you can run the app in dev mode or in build mode using the following commands:
```
gulp serve-dev

gulp serve-build

```