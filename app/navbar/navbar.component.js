(function () {
    'use strict';

    angular
        .module('app')
        .component('navbar', navbar());


    function navbar() {
        function navbarController($state) {
            var $ctrl = this;
            $ctrl.active = active;


            function active(state){
                return $state.includes(state)
            }


        }
        /* @ngInject */
        navbarController.$inject = ['$state'];
        return {
            bindings: {
            },
            controller: navbarController,
            templateUrl: 'app/navbar/navbar.html',
            controllerAs: '$ctrl'
        }
    }

} ());