(function () {
    'use strict';

    angular
        .module('app')
        .component('page', page());


    function page() {

        function pageController(swapiService,$stateParams,$state) {
            var $ctrl = this;
            $ctrl.nextPage = nextPage;
            $ctrl.prevPage = prevPage;

   
            $ctrl.$onInit = init;

            function nextPage(){
                if($ctrl.pageData && $ctrl.pageData.next){
                    var page = angular.isDefined($stateParams.page) ? (parseInt($stateParams.page)+1) : 2;
                    $state.go($ctrl.type+'.page', {page: page});
                }
            }
            function prevPage(){
                if($ctrl.pageData && $ctrl.pageData.previous){
                    $state.go($ctrl.type+'.page', {page: parseInt($stateParams.page)-1});
                }
            }

            function init() {
                var page = angular.isDefined($stateParams.page) ? $stateParams.page : null;
                var id = angular.isDefined($stateParams.id) ? $stateParams.id : null;
                $ctrl.hasId = !!id ? true : false;
                $ctrl.isSearching = true;
                switch ($ctrl.type) {
                    case 'people':
                        swapiService.people(id, page)
                        .then(response)
                        .catch(errorHandler);
                        break;
                    case 'films':
                        swapiService.films(id, page)
                        .then(response)
                        .catch(errorHandler);
                        break;
                    case 'planets':
                        swapiService.planets(id, page)
                        .then(response)
                        .catch(errorHandler);
                        break;
                    case 'species':
                        swapiService.species(id, page)
                        .then(response)
                        .catch(errorHandler);
                        break;
                    case 'starships':
                        swapiService.starships(id, page)
                        .then(response)
                        .catch(errorHandler);
                        break;
                    case 'vehicles':
                        swapiService.vehicles(id, page)
                        .then(response)
                        .catch(errorHandler);
                        break;

                }//switch
            }//init
            function response(data){
                $ctrl.pageData = data;
                $ctrl.isSearching = false;
            }
            function errorHandler(err){
                $ctrl.errorStatus = err.status;
                $ctrl.isSearching = false;   
            }

        }
        /* @ngInject */
        pageController.$inject = ['swapiService','$stateParams','$state'];
        return {
            bindings: {
                type: '<'
            },
            controller: pageController,
            templateUrl: 'app/page/page.html',
            controllerAs: '$ctrl'
        }
    }

} ());