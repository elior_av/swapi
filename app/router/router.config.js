(function () {
  'use strict';

  angular
    .module('app')
    .config(routerConfig);

  /* @ngInject */
  routerConfig.$inject = ['$stateProvider', '$locationProvider', '$urlRouterProvider'];
  function routerConfig($stateProvider, $locationProvider, $urlRouterProvider) {
    $stateProvider
      .state('home', {
        url: '/',
        template: '<home></home>',
      })
      .state('people', {
        url: '/people',
        template: '<ui-view/>',
      })
      .state('people.page', {
        url: '/?page',
        template: '<page type="\'people\'"></page>',
      })
      .state('people.id', {
        url: '/:id',
        template: '<page type="\'people\'"></page>',
      })
    
      .state('planets', {
        url: '/planets',
        template: '<ui-view/>',
      })
      .state('planets.page', {
        url: '/?page',
        template: '<page type="\'planets\'"></page>',
      })
      .state('planets.id', {
        url: '/:id',
        template: '<page type="\'planets\'"></page>',
      })

      .state('starships', {
        url: '/starships',
        template: '<ui-view/>',
      })
      .state('starships.page', {
        url: '/?page',
        template: '<page type="\'starships\'"></page>',
      })
      .state('starships.id', {
        url: '/:id',
        template: '<page type="\'starships\'"></page>',
      })

      .state('vehicles', {
        url: '/vehicles',
        template: '<ui-view/>',
      })
      .state('vehicles.page', {
        url: '/?page',
        template: '<page type="\'vehicles\'"></page>',
      })
      .state('vehicles.id', {
        url: '/:id',
        template: '<page type="\'vehicles\'"></page>',
      })

      .state('species', {
        url: '/species',
        template: '<ui-view/>',
      })
      .state('species.page', {
        url: '/?page',
        template: '<page type="\'species\'"></page>',
      })
      .state('species.id', {
        url: '/:id',
        template: '<page type="\'species\'"></page>',
      })

      .state('films', {
        url: '/films',
        template: '<ui-view/>',
      })
      .state('films.page', {
        url: '/?page',
        template: '<page type="\'films\'"></page>',
      })
      .state('films.id', {
        url: '/:id',
        template: '<page type="\'films\'"></page>',
      });

    $urlRouterProvider.otherwise('/');

    
    // $locationProvider.html5Mode({
    //   enabled: true,
    //   requireBase: true
    // });

  }

} ());
