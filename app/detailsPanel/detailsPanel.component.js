(function () {
    'use strict';

    angular
        .module('app')
        .component('detailsPanel', detailsPanel());


    function detailsPanel() {

        function detailsPanelController(swapiService, functionsService, $state, localStorageHelperService, logger) {
            var $ctrl = this;
            $ctrl.toggle = toggle;
            $ctrl.changeExtendType = changeExtendType;
            $ctrl.tabActive = tabActive;
            $ctrl.goToItemPage = goToItemPage;
            $ctrl.goToUrl = goToUrl;
            $ctrl.toggleFavorite = toggleFavorite;
            $ctrl.inFavoriteList = localStorageHelperService.inFavoriteList($ctrl.type, $ctrl.data.url);
            $ctrl.extend = false;
            $ctrl.extendType = $ctrl.type == 'films' ? 'planets' : 'films';
            $ctrl.extendData = null;
            $ctrl.error = null;
            $ctrl.isSearching = false;

            init();

            function toggle() {
                $ctrl.extend = !$ctrl.extend;
                if (!$ctrl.extendData && $ctrl.extend) {
                    getAllData();
                }
            }
            function toggleFavorite() {
                if ($ctrl.inFavoriteList) {
                    localStorageHelperService.remove($ctrl.type, $ctrl.data.url);
                    logger.info("the item was removed from your favorite list");
                } else {
                    localStorageHelperService.add($ctrl.type, $ctrl.data.url);
                    logger.success("the item was added to your favorite list");
                }
                $ctrl.inFavoriteList = !$ctrl.inFavoriteList;
            }
            function getAllData() {
                $ctrl.isSearching = true;
                $ctrl.extendData = null;
                $ctrl.error = null;
                swapiService.requestMultiUrlsAndTypes(createReqForAllData())
                    .then(function (res) {
                        $ctrl.extendData = res;
                        $ctrl.isSearching = false;
                    })
                    .catch(function (err) {
                        $ctrl.error = err;
                        $ctrl.isSearching = false;
                    });
            }
            function changeExtendType(type) {
                $ctrl.extendType = type;
            }
            function goToItemPage() {
                goToUrl($ctrl.type, $ctrl.data.url);
            }
            function goToUrl(type, url) {
                var itemId = functionsService.getIdFromUrl(url);
                $state.go(type + '.id', { id: itemId });
            }
            function tabActive(type) {
                return $ctrl.extendType == type ? 'active' : '';
            }
            function createReqForAllData() {
                switch ($ctrl.type) {
                    case 'people':
                        return {
                            films: $ctrl.data.films,
                            species: $ctrl.data.species,
                            vehicles: $ctrl.data.vehicles,
                            starships: $ctrl.data.starships,
                            homeworld: [$ctrl.data.homeworld]
                        }
                    case 'planets':
                        return {
                            films: $ctrl.data.films,
                            residents: $ctrl.data.residents
                        }
                    case 'films':
                        return {
                            characters: $ctrl.data.characters,
                            species: $ctrl.data.species,
                            vehicles: $ctrl.data.vehicles,
                            starships: $ctrl.data.starships,
                            planets: $ctrl.data.planets,
                        }
                    case 'species':
                        return {
                            films: $ctrl.data.films,
                            people: $ctrl.data.people,
                            homeworld: [$ctrl.data.homeworld]
                        }
                    case 'starships':
                    case 'vehicles':
                        return {
                            films: $ctrl.data.films,
                            pilots: $ctrl.data.pilots,
                        }
                }
            }
            function init() {
                if ($ctrl.mode) {
                    getAllData();
                }
            }







        }
        /* @ngInject */
        detailsPanelController.$inject = ['swapiService', 'functionsService', '$state', 'localStorageHelperService', 'logger'];
        return {
            bindings: {
                data: '<',
                type: '<',
                mode: '<'
            },
            controller: detailsPanelController,
            templateUrl: 'app/detailsPanel/detailsPanel.html',
            controllerAs: '$ctrl'
        }
    }

} ());