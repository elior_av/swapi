(function () {
    'use strict';

    angular
        .module ('app')
        .component ('home', home());


    function home() {

        function homeController(localStorageHelperService){
            var $ctrl = this;
            $ctrl.isSearching = true;
            $ctrl.favorite = null;
            $ctrl.error = null;
            
            $ctrl.$onInit = function(){
                localStorageHelperService.getFullList()
                .then(function(list){
                    $ctrl.favorite = list;
                    $ctrl.isSearching = false;
                })
                .catch(function(err){
                    $ctrl.error = err;
                    $ctrl.isSearching = false;
                });
            }
        }

        /* @ngInject */
        homeController.$inject = ['localStorageHelperService'];
        return {
            bindings: {},
            controller: homeController,
            templateUrl: 'app/home/home.html',
            controllerAs: '$ctrl'
        }
    }

} ());