(function() {
'use strict';

    angular
        .module('app')
        .factory('functionsService', functionsService);

    /* @ngInject */
    functionsService.$inject = [];
    function functionsService() {
        var functionsService = {
           getIdFromUrl: getIdFromUrl
        };
        
        return functionsService;

        ////////////////
        
        function getIdFromUrl(url){
            var arr = url.split('/');
            return arr[arr.length-2];
        }

    }
})();