(function () {
    'use strict';

    angular
        .module('app')
        .factory('swapiService', Service);

    /* @ngInject */
    Service.$inject = ['$http','$q'];
    function Service($http,$q) {
        var baseUrl = 'http://swapi.co/api/';

        var service = {
            request: request,
            requestMultiUrlsAndTypes: requestMultiUrlsAndTypes,
            films: films,
            people: people,
            planets: planets,
            species: species,
            starships: starships,
            vehicles: vehicles  
        };

        return service;

        ////////////////



        function request(url) {
            return $http.get(url).
                then(
                function Î(data) {
                    if (data.status !== 200) {
                        // something goes wrong
                        throw new Error(data);
                    }
                    return data.data;
                }, function (err) {
                    throw err;
                }
                );
        }
        function requestMultiUrlsAndTypes(list) {
            var obj = {};
            angular.forEach(list, function (value, key) {
                this[key] = requestMultiUrls(value)
            }, obj);
            return $q.all(obj);
        }
        function requestMultiUrls(list) {
            var arr = [];
            for (var i=0; i<list.length; i++) {
                arr.push(request(list[i]));
            }
            return $q.all(arr);
        }
        function createUrl(type, id, page) {
            var pageParam = '?page=';
            var url = baseUrl + type + '/';

            if (angular.isDefined(id) && id != null) {
                url += id + '/';
            } else if (angular.isDefined(page) && page != null) {
                url += pageParam + page;
            }
            return url;
        }
        function getResource(type, id, page) {
            return request(createUrl(type, id, page));
        }

        function films(id, page) {
            return getResource('films', id, page);
        }

        function people(id, page) {
            return getResource('people', id, page);
        }

        function planets(id, page) {
            return getResource('planets', id, page);
        }

        function species(id, page) {
            return getResource('species', id, page);
        }

        function starships(id, page) {
            return getResource('starships', id, page);
        }

        function vehicles(id, page) {
            return getResource('vehicles', id, page);
        }

    }
})();