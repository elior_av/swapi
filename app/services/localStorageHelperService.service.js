(function () {
    'use strict';

    angular
        .module('app')
        .factory('localStorageHelperService', localStorageHelperService);

    /* @ngInject */
    localStorageHelperService.$inject = ['localStorageService','swapiService'];
    function localStorageHelperService(localStorageService,swapiService) {

        var lastTimeFavorite = localStorageService.get('lastTimeFavorite');
        var favorite = {
            people: [],
            films: [],
            planets: [],
            species: [],
            starships: [],
            vehicles: []
        };
        favorite = !lastTimeFavorite ? favorite : lastTimeFavorite;
        localStorageService.set('lastTimeFavorite', favorite);

        var localStorageHelperService = {
            add: add,
            remove: remove,
            getFavorite: getFavorite,
            inFavoriteList: inFavoriteList,
            getFullList: getFullList
        };

        return localStorageHelperService;

        ////////////////

        function add(type, url) {
            if (favorite[type].indexOf(url) === -1) {
                favorite[type].push(url);
                localStorageService.set('lastTimeFavorite', favorite);
            }
        }
        function remove(type, url) {
            if (favorite[type].indexOf(url) !== -1) {
                favorite[type].slice(favorite[type].indexOf(url), 1);
                localStorageService.set('lastTimeFavorite', favorite);
            }
        }
        function getFavorite() {
            return favorite;
        }
        function inFavoriteList(type, url) {
            return favorite[type].indexOf(url) !== -1;
        }
        function getFullList() {
            return swapiService.requestMultiUrlsAndTypes(favorite);
        }



    }
})();