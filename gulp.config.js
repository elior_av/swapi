module.exports = function() {
    var root = './';
    var app = root + 'app/';
    var wiredep = require('wiredep');
    var bowerFiles = wiredep({devDependencies: true})['js'];
    var bower = {
        json: require('./bower.json'),
        directory: './bower_components/',
        ignorePath: '../..'
    };
    var nodeModules = 'node_modules';

    var config = {
        index: root + 'index.html',
        root : root,
        js: [
            app + '**/*.modules.js',
            app + '**/*.js'
        ],
        css: app + '**/*.css',
        build: root + 'build/',
        html: app + '**/*.html',
        temp: root + '.tmp/',
        htmltemplates: app + '**/*.html',
        fonts: [
            bower.directory + '**/*.otf',
            bower.directory + '**/*.svg',
            bower.directory + '**/*.ttf',
            bower.directory + '**/*.woff',
            bower.directory + '**/*.woff2',
            bower.directory + '**/*.eot',
        ],
        /**
         * browser sync
         */
        // browserReloadDelay: 1000,
        /**
         * template cache
         */
        templateCache: {
            file: 'templates.js',
            options: {
                module: 'app',
                root: 'app/',
                standAlone: false
            }
        },
        /**
         * Bower and NPM files
         */
        bower: bower,
        packages: [
            './package.json',
            './bower.json'
        ],
    };

    /**
     * wiredep and bower settings
     */
    config.getWiredepDefaultOptions = function() {
        var options = {
            bowerJson: config.bower.json,
            directory: config.bower.directory,
            ignorePath: config.bower.ignorePath
        };
        return options;
    };

    return config;

    ////////////////

};